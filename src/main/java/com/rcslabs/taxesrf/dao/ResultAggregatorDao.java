package com.rcslabs.taxesrf.dao;

import com.rcslabs.taxesrf.domain.ResultAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ResultAggregatorDao {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<ResultAggregator> getResultAggregators(String query) {
        return jdbcTemplate.query(query, (resultSet, i) -> getResultAggregator(resultSet));
    }

    private ResultAggregator getResultAggregator(ResultSet resultSet) throws SQLException {
        ResultAggregator resultAggregator = new ResultAggregator();
        resultAggregator.setRow(resultSet.getString("row"));
        resultAggregator.setColumn(resultSet.getString("col"));
        resultAggregator.setValue(resultSet.getInt("val"));
        return resultAggregator;
    }
}