package com.rcslabs.taxesrf.dao;

import com.rcslabs.taxesrf.domain.ResultAggregator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ContextConfiguration(classes = {ConfigDaoTest.class, ResultAggregatorDao.class})
@ExtendWith(SpringExtension.class)
@Sql(scripts = {"/defineTable.sql"})
class ResultAggregatorDaoTest {
    @Autowired
    private ResultAggregatorDao resultAggregatorDao;

    @Test
    public void getResultAggregators() {
        String query = "SELECT a AS \"row\", b AS col, sum(v) AS val FROM source_data GROUP BY a,b";
        List<ResultAggregator> resultAggregators = resultAggregatorDao.getResultAggregators(query);
        System.out.println(resultAggregators);
        resultAggregators.stream().forEach((result) -> assertEquals("Налоги на имущество", result.getRow(), "There is only: Налоги на имущество as row"));
        assertEquals("Земельный налог", resultAggregators.get(0).getColumn(), "The first result`s column expected as 'Земельный налог'");
    }

    @Test
    public void getResultAggregatorsFromNullQuery() {
        assertThrows(IllegalArgumentException.class, () -> resultAggregatorDao.getResultAggregators(null));
    }

    @Test
    public void getResultAggregatorsFromEmptyQuery() {
        assertThrows(UncategorizedSQLException.class, () -> resultAggregatorDao.getResultAggregators(""));
    }

    @Test
    public void getResultAggregatorsFromQueryWithWrongParameters() {
        String query = "SELECT WRONG1 AS \"row\", WRONG2 AS col, sum(v) AS val FROM source_data GROUP BY WRONG1,WRONG2";
        assertThrows(BadSqlGrammarException.class, () -> resultAggregatorDao.getResultAggregators(query));
    }
}