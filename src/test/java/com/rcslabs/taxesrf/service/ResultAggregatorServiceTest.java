package com.rcslabs.taxesrf.service;

import com.rcslabs.taxesrf.dao.ResultAggregatorDao;
import com.rcslabs.taxesrf.domain.ResultAggregator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class ResultAggregatorServiceTest {
    @Mock
    private ResultAggregatorDao resultAggregatorDao;
    @InjectMocks
    private ResultAggregatorService resultAggregatorService;

    @BeforeEach
    public void callPostServiceConstructMethod() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method postConstruct = ResultAggregatorService.class.getDeclaredMethod("initAllowedParameters", null); // methodName,parameters
        postConstruct.setAccessible(true);
        postConstruct.invoke(resultAggregatorService);
    }

    @Test
    void getResultAggregators() {
        String query = "SELECT a AS row, b AS col, sum(v) AS val FROM source_data GROUP BY a, b;";
        List<ResultAggregator> resultAggregators = new ArrayList<>(asList(new ResultAggregator()));
        when(resultAggregatorDao.getResultAggregators(query)).thenReturn(resultAggregators);
        assertEquals(resultAggregators, resultAggregatorService.getResultAggregators("a", "b"), "Expected list not returned");
    }

    @Test
    void getResultAggregatorsByNotAllowedParameters() {
        assertThrows(IllegalArgumentException.class, () -> resultAggregatorService.getResultAggregators("WRONG", "WRONG"));
    }

    @Test
    void getResultAggregatorsByNullParameters() {
        assertThrows(IllegalArgumentException.class, () -> resultAggregatorService.getResultAggregators(null, null));
    }
}