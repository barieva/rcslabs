package com.rcslabs.taxesrf.web;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ErrorController {
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public String handleError(Exception exception) {
        return "{\"error\": \"" + exception.getMessage() + "\"}";
    }
}