package com.rcslabs.taxesrf.domain;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ResultAggregator {
    private String row;
    @JsonAlias("col")
    private String column;
    @JsonAlias("val")
    private int value;
}