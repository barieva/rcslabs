package com.rcslabs.taxesrf.web;

import com.rcslabs.taxesrf.domain.ResultAggregator;
import com.rcslabs.taxesrf.service.ResultAggregatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ResultAggregatorController {
    ResultAggregatorService resultAggregatorService;

    @Autowired
    public void setResultAggregatorService(ResultAggregatorService resultAggregatorService) {
        this.resultAggregatorService = resultAggregatorService;
    }

    @GetMapping("/row={row}&col={col}")
    public List<ResultAggregator> aggregate(@PathVariable("row") String row, @PathVariable("col") String column) {
        return resultAggregatorService.getResultAggregators(row, column);
    }
}