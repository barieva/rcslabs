package com.rcslabs.taxesrf.service;

import com.rcslabs.taxesrf.dao.ResultAggregatorDao;
import com.rcslabs.taxesrf.domain.ResultAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;

@Service
public class ResultAggregatorService {
    private ResultAggregatorDao resultAggregatorDao;
    private Set<String> allowedParameters;

    @PostConstruct
    private void initAllowedParameters() {
        allowedParameters = new HashSet<>(asList("a", "b", "c", "d", "y"));
    }

    @Autowired
    public void setResultAggregatorDao(ResultAggregatorDao resultAggregatorDao) {
        this.resultAggregatorDao = resultAggregatorDao;
    }

    @Transactional(readOnly = true)
    public List<ResultAggregator> getResultAggregators(String row, String column) {
        if (areParametersAllowed(row, column)) {
            String query = createQuery(row, column);
            return resultAggregatorDao.getResultAggregators(query);
        } else {
            throw new IllegalArgumentException("Passed wrong parameter/s");
        }
    }

    private String createQuery(String row, String column) {
        return "SELECT " + row + " AS row, " + column + " AS col, sum(v) AS val FROM source_data GROUP BY " + row + ", " + column + ';';
    }

    private boolean areParametersAllowed(String firstParameter, String secondParameter) {
        return allowedParameters.contains(firstParameter) && allowedParameters.contains(secondParameter);
    }
}