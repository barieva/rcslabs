package com.rcslabs.taxesrf.controller;

import com.rcslabs.taxesrf.domain.ResultAggregator;
import com.rcslabs.taxesrf.service.ResultAggregatorService;
import com.rcslabs.taxesrf.web.ResultAggregatorController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class ResultAggregatorControllerTest {
    @Mock
    private ResultAggregatorService resultAggregatorService;
    @InjectMocks
    private ResultAggregatorController resultAggregatorController;

    @Test
    void getResultAggregators() {
        List<ResultAggregator> resultAggregators = new ArrayList<>(asList(new ResultAggregator()));
        when(resultAggregatorService.getResultAggregators("a", "b")).thenReturn(resultAggregators);
        assertEquals(resultAggregators, resultAggregatorController.aggregate("a", "b"), "Expected list not returned");
    }

    @Test
    void getResultAggregatorsByNotAllowedParameters() {
        when(resultAggregatorService.getResultAggregators("WRONG", "WRONG")).thenThrow(IllegalArgumentException.class);
        assertThrows(IllegalArgumentException.class, () -> resultAggregatorService.getResultAggregators("WRONG", "WRONG"));
    }

    @Test
    void getResultAggregatorsByNullParameters() {
        when(resultAggregatorService.getResultAggregators(null, null)).thenThrow(IllegalArgumentException.class);
        assertThrows(IllegalArgumentException.class, () -> resultAggregatorService.getResultAggregators(null, null));
    }
}